#!/usr/bin/python
#-*- coding: utf-8-*

# Autor: Chris Rätsepso, 2016

# Järgneva skripti eesmärgiks on lugeda sisendfailist URL-id ja String-id (iga URL-le vastab kindel String, mis asub samal real), 
# teha päring vastava URL-i pihta, lugeda sisse vastava URL-i lähtekood ning lähtekoodist otsida vastavat Stringi ning
# lõpuks kirjutada väljundfaili vastav URL, String ning kas vastav String leidus lähtekoodis või mitte.


# Imporditakse vajalikud moodulid, mida skripti töös vaja läheb

import os.path
import sys
import urllib2

# Salvestatakse kasutaja poolt sisestatud parameetrite arv muutujasse

parameetrid = len(sys.argv)

# Kontrollitakse, kas kasutaja sisestas õige arvu parameetreid

if parameetrid != 3:
    print "Kasutamine: %s Sisendfail Väljundfail" % sys.argv[0]
    sys.exit()

# Salvestatakse kasutaja poolt sisestatud sisendfail ja väljundfail muutujatesse

sisendfail = sys.argv[1]
valjundfail = sys.argv[2]

# Kontrollitakse, kas sisendfail on olemas

sisend_olemas = os.path.isfile(sisendfail)

if sisend_olemas == False:
    print "Sisendfaili pole olemas!"
    sys.exit()

# Kontrollitakse, kas väljundfail on olemas 

valjund_olemas = os.path.isfile(valjundfail)
if valjund_olemas == False:
    print "Väljundfaili pole olemas!"
	sys.exit()
  
sisend = open(sisendfail)
sisu = []

# Sisendfailist loetakse kõik read faili ja salvestatakse list-i

for rida in sisend.read().splitlines():
    read = rida.split(" ")
    sisu.append(read)
sisend.close()   

""" Listi elemendid käiakse läbi ükshaaval ,salvestatakse URL-id ja string-id eraldi muutujatesse.
 Tehakse URL-i pihta päring, luuakse lähtekoodist objekt ning
 otsitakse vastavat string-i lähtekoodi objektist ning otsingu tulemus salvestatakse muutujasse
 Täidetakse väljundfail vastaval sellele, kas leiti lähtekoodist vastav string või mitte
 """
f = open (valjundfail, 'w')
 
for element in sisu:
    url = element [0]
    string = element [1] 
	
    paring = urllib2.urlopen(url)
    lahtekood = paring.read()
    tulemus = lahtekood.find(string)
	
    if tulemus != -1:
       f.write (url + " " + string +' JAH \n')
    else:
       f.write (url + " " + string + ' EI \n')

f.close()